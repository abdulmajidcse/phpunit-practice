<?php

namespace Abdulmajidcse\Phpunit;

class CodeMask
{
    /**
     * Generate hidden phone number
     * 
     * @param string $phoneNumber
     * @param int $visibleStartDigits
     * @param int $visibleEndDigits
     * 
     * @return string
     */
    public function hidePhoneNumber(string $phoneNumber, int $visibleStartDigits = 1, int $visibleEndDigits = 1): string
    {
        $length = strlen($phoneNumber);

        if ($length <= $visibleStartDigits + $visibleEndDigits) {
            // do not need to hide phone number
            return $phoneNumber;
        }

        $firstDigits = substr($phoneNumber, 0, $visibleStartDigits);
        $lastDigits = substr($phoneNumber, -$visibleEndDigits);
        $middlePortion = str_repeat('*', $length - $visibleStartDigits - $visibleEndDigits);

        return $firstDigits . $middlePortion . $lastDigits;
    }

    /**
     * Generate formatted serial
     * 
     * @param string $string
     * @param int $length
     * @param string $padString
     * @param string $prefix
     * 
     * @return string
     */
    public function generateSerial(string $string, int $length = 10, string $padString = '0', string $prefix = ''): string
    {
        return $prefix . str_pad($string, $length, $padString, STR_PAD_LEFT);
    }
}
