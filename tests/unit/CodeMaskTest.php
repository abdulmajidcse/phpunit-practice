<?php

use Abdulmajidcse\Phpunit\CodeMask;
use PHPUnit\Framework\TestCase;

final class CodeMaskTest extends TestCase
{
    protected CodeMask $codeMask;

    protected function setUp(): void
    {
        $this->codeMask = new CodeMask();
    }

    public function testHidePhoneNumber()
    {
        $hiddenPhoneNumber = $this->codeMask->hidePhoneNumber("1234567890", 2, 2);
        $this->assertEquals('12******90', $hiddenPhoneNumber);
    }

    public function testGenerateSerial()
    {
        $serial = $this->codeMask->generateSerial(123, 10, 0, '2701');
        $this->assertEquals('27010000000123', $serial);
    }
}
